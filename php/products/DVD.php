<?php

/*Product child class. Can use parent functions and use it own functions and variables if necessary.*/

class DVD extends Product
{
    private string $attribute;

    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /*Create attribute field*/
    public function setAttribute(array $attributesArray): void
    {
        $attributeString = 'Size: ' . implode(' Mb', $attributesArray);
        $this->attribute = $attributeString;
    }
}