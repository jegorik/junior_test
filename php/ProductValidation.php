<?php

/**
 * Class ProductValidation
 * This class validates information from web form
 */
class ProductValidation extends Resolver implements Validator
{
    /*This variable contains validation status. By default false.*/
    private bool $validationStatus = false;

    /*Public function to call ProductValidation procedure outside class. Receives input fields data.*/
    public function makeValidation($formData): object
    {
        return $this->doFormValidation($formData);
    }

    /*This function starts form input fields data validation procedure. */
    private function doFormValidation(array $formData): object
    {
        /*Creating new object of the class Warehouse to get access for public function, to receive array with stored products name.*/
        unset($formData['process']);
        $warehouse = new \Warehouse();
        /*Storing received array in new variable.*/
        $products = $warehouse->getProducts();
        /*Empty array for storing errors information in validation process.*/
        $errors = array();
        /*Empty array for storing information about validation steps. If validation step successfully complete - stores true value, otherwise false.*/
        $validationStepsResult = array();
        /*Empty variable for upgrading 'attribute' field information to future store in database.*/
        $product = '';
        /*This empty array will stores "clear" input field data, after validation process.*/
        $validDataArray = array();
        $counter = 0;
        /*Loop to check input fields data.*/
        foreach ($formData as $fieldName => $fieldInputData) {
            /*New variable to store input field data for future validation process.*/
            $data = $fieldInputData;
            /*ProductValidation steps to prevent "injection" in input fields data.*/
            $data = htmlspecialchars($data);
            $data = trim($data);
            $data = strip_tags($data);
            $data = stripslashes($data);
            /*Checking input field data for empty or null value.*/
            if (empty($data) || $data === null) {
                /*If true - save error information in $errors array.*/
                $errors[] = $fieldName . ' field is empty or null!';
                /*Make this validation step false.*/
                $validationStepsResult[] = false;
                /*Check "Type Switcher" field value. This step try to prevent a "wrong product" "injection".*/
            } elseif ($fieldName === 'productType') {
                /*If product name not found in Warehouse class array, then validation step false.*/
                if (!array_key_exists($data, $products)) {
                    $errors[] = $data . ' wrong product in "Type Switcher" field!';
                    $validationStepsResult[] = false;
                    return $errors;
                }
            } else {
                /*If condition to check input field data by regex patterns using fieldInputValidation() function.
                If step successfully - save true value in $validationStepsResult[] array, otherwise save error data in $errors[] array. Also it creates
                attribute fields.*/
                if ($this->fieldInputValidation($data, $fieldName)) {
                    $validationStepsResult[] = true;

                    foreach ($products as $key => $value) {
                        if (strpos($value, $fieldName) !== false) {
                            $string = 'attribute' . $counter;
                            $validDataArray[$string] = $data;
                            $counter++;
                        }
                    }
                } else {
                    $errors[] = $fieldName . ' ' . $data . ' not allowed value!';
                }
            }
            /*After validation steps, stores "clear" and "correct" input field data to a new array ($validDataArray).*/
            $validDataArray[$fieldName] = $data;
        }
        /*Create product with valid data.*/
        $product = $this->createProduct($validDataArray);
        if (!in_array(false, $validationStepsResult, true)) {
            $this->validationStatus = true;
            return $product;
        }
        return $errors;
    }

    /*Function to check input fields regex, using patters, stored in $patternContainer array.*/
    private function fieldInputValidation(string $data, string $fieldName): bool
    {
        /*Creating new variable to store correct pattern for a input field data, using getPattern() function.*/
        $pattern = $this->getPattern($fieldName);
        /*Check input field data using patterns by preg_match() function. If step successfully returns 1, otherwise null.*/
        return preg_match($pattern, $data);
    }

    /*Function to get patterns from $patternContainer array for regex validation.*/
    private function getPattern(string $fieldName): string
    {
        /*Array to store patterns for input fields regex check.*/
        $patternContainer = [
            'sku' => '/^([\d+\w+]{3,20})$/',
            'name' => '/^([\w+\W+]{2,50})$/',
            'price' => '/^(\d{1,8})(,\d{1,2})?$/',
            'size' => '/^(\d{1,5})(,\d{1,3})?$/',
            'weight' => '/^(\d{1,5})(,\d{1,3})?$/',
            'height' => '/^(\d{1,4})(,\d{1,3})?$/',
            'width' => '/^(\d{1,4})(,\d{1,3})?$/',
            'length' => '/^(\d{1,4})(,\d{1,3})?$/'
        ];
        /*Returns pattern according input field name.*/
        return $patternContainer[$fieldName];
    }

    /*Function to update attribute field information. Returns attribute string with additional information.*/
    private function createProduct(array $validDataArray): object
    {
        /*Empty array for attributes values*/
        $attributesArray = [];
        /*Loop with regex check to find array fields containing word 'attribute'.*/
        foreach ($validDataArray as $fieldName => $data) {
            if (preg_match('~\w*attribute\w*~', $fieldName)) {
                $attributesArray[] = $data;
            }
        }
        /*Create new product with valid data.*/
        $product = new $validDataArray['productType']();
        $product->setSku($validDataArray['sku']);
        $product->setName($validDataArray['name']);
        $product->setPrice($validDataArray['price']);
        $product->setType($validDataArray['productType']);
        $product->setAttribute($attributesArray);
        return $product;
    }

    /*This getter returns validation status.*/
    public function isValidationStatus(): bool
    {
        return $this->validationStatus;
    }

}