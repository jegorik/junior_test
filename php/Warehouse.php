<?php

/*This class stores information about products in the database.*/

class Warehouse
{
    /*Array with products name and attributes field names.*/
    private const Products = array('DVD' => 'size', 'Book' => 'weight', 'Furniture' => 'height width length');
    /*Public getter to get private variable of the Warehouse class (Products array).*/
    public function getProducts(): array
    {
        return self::Products;
    }
}