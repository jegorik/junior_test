<?php

/*Connect classLoader script.*/
require_once 'classLoader.php';

/*Creating new object of a class Resolver to start processing input fields data.*/
$resolver = new Resolver();
/*Receiving form data by POST function and putting it to an array.*/
$formData = $_POST['product'];
$result = null;
/*Checking if data from POST function is empty.*/
if (!empty($formData)) {
    /*If not empty - run function (function name stored in $formData 'process' => 'functionName').*/
    $function = $formData['process'];
    echo json_encode($resolver->$function($formData), JSON_THROW_ON_ERROR);
} else {
    /*POST is empty. Returns error.*/
    echo json_encode($result['Empty POST data.'], JSON_THROW_ON_ERROR);
}

class Resolver
{

    /*Function to make form data validation.*/
    public function startValidation(array $formData): array
    {
        /*Creating new object of a class ProductValidation to use it public functions.*/
        $validation = new \ProductValidation();
        /*Stores validation result in new variable.*/
        $validationResult = $validation->makeValidation($formData);
        /*Check validation status.*/
        $validationStatus = $validation->isValidationStatus();
        /*Depending of $validationStatus make next steps. If true, run function doSave() to save form data to database,
        otherwise returns errors.*/
        if ($validationStatus) {
            return (new Database())->doSave($validationResult);
        }
        return (array)$validationResult;
    }

    /*This function starts product data loading procedure.*/
    public function loadDataFromDatabase($formData): array
    {
        return (new Database())->loadData();
    }

    /*This function removes product data from database.*/
    public function deleteDataFromDatabase($formData): string
    {
        return ((new Database())->deleteData($formData));
    }
}